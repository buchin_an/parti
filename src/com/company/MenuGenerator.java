package com.company;

import java.util.HashMap;
import java.util.Map;

public final class MenuGenerator {

    private MenuGenerator() {
    }

    public static HashMap<String, Integer> generate() {
        Map<String, Integer> menuMap = new HashMap<>();
        menuMap.put("a", 1000);
        menuMap.put("b", 1000);
        menuMap.put("c", 1000);
        menuMap.put("d", 1000);
        menuMap.put("e", 1000);
        menuMap.put("f", 1000);
        menuMap.put("g", 1000);
        menuMap.put("h", 1000);
        menuMap.put("i", 1000);
        menuMap.put("j", 1000);
        menuMap.put("k", 1000);
        menuMap.put("l", 1000);

        return (HashMap<String, Integer>) menuMap;
    }


}
