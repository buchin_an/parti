package com.company;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    private static HashMap<String, Integer> menu = MenuGenerator.generate();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> bill = new HashMap<>();

        boolean flag = true;

        while (flag) {
            System.out.println("choose");

            String dish = scanner.nextLine();
            if (dish.equalsIgnoreCase("q")) {
                flag = false;
            }

            if (menu.containsKey(dish)) {

                if (!bill.containsKey(dish)) {
                    bill.put(dish, menu.get(dish));
                } else if (bill.containsKey(dish)) {
                    Integer tmp = bill.get(dish);
                    bill.put(dish, tmp + menu.get(dish));
                }
            }
        }
        int total = 0;
        System.out.println("bill:");
        for (Map.Entry<String, Integer> entry : bill.entrySet()) {
            if (entry.getValue() != null) {
                total += entry.getValue();
            }
            System.out.print(entry.getKey() + ": ");
            System.out.println(entry.getValue());

        }
        System.out.println("total " + total);


    }
}
